//******************************************************************************
// file: menuDS1WM.cc
// desc: DS1WM Interface - menu program
// auth: 18-AUG-2008 Ralf SPIWOKS
//******************************************************************************

#include <DS1WM/DS1WM.h>
#include "RCDMenu/RCDMenu.h"

#include <cstdio>
#include <cstdlib>
#define u_int unsigned int

using namespace LVL1;

RCD::VME* vme;
RCD::VMEMasterMap* vmm;
DS1WM* module;

//------------------------------------------------------------------------------

class DS1WMOpen : public RCD::MenuItem {
  public:
    DS1WMOpen() { setName("open DS1WM"); };
    int action() {

        DS1WM::MODULE_TYPE m;

        // open VME driver/library
        vme = RCD::VME::Open();

        // open VME master mapping
        int sel = enterInt("Module type (1-MIOCT, 2-MIROD, 3-MICTP, 4-CTPCOREPLUS, 5-CTPOUTPLUS, 6-ALTI)", 0, 6);
        switch (sel) {
        case 1: {       // MIOCT
                m = DS1WM::MIOCT;
                int slot = enterInt("Slot number: ", 4, 21);
                u_int vme_addr = ((slot & 0x1F) << 24) | 0x800000;
                vmm = vme->MasterMap(vme_addr, 0x100000, 2, 0);
            }
            break;

        case 2: {       // MIROD
                m = DS1WM::MIROD;
                vmm = vme->MasterMap(0x68000000, 0x80000, 2, 0);
            }
            break;

        case 3: {       // MICTP
                m = DS1WM::MICTP;
                vmm = vme->MasterMap(0x60000000, 0x80000, 2, 0);
            }
            break;

        case 4: {       // CTPCOREPLUS
                m = DS1WM::CTPCOREPLUS;
                vmm = vme->MasterMap(0x58000000, 0x00210000, 2, 0);
            }
            break;

        case 5: {       // CTPOUTPLUS
                m = DS1WM::CTPOUTPLUS;
                int slot = enterInt("Slot number: ", 12, 17);
                if (slot == 16) exit(0);
                u_int vme_addr = (slot & 0x1F) * 0x8000000;
                vmm = vme->MasterMap(vme_addr, 0x00040000, 2, 0);
            }
            break;

        case 6: {       // ALTI
                m = DS1WM::ALTI;
                u_int p = (u_int)enterHex("Vmebus_address   (hex)",0,0xffffffff);
                if(p < 0x100) p = p << 24;
                vmm = vme->MasterMap(p,0x01000000,2,0);
            }
            break;
        default: {
                m = DS1WM::UNKNOWN;
                u_int p = (u_int)enterHex("Vmebus_address   (hex)",0,0xffffffff);
                if(p < 0x100) p = p << 24;
                u_int s = (u_int)enterHex("Window_size      (hex)",0,0xffffffff);
                u_int a = (u_int)enterHex("Address_modifier (hex)",0,0xffffffff);
                vmm = vme->MasterMap(p,s,a,0);
            }
        }

        // create DS1WM
        module = new DS1WM(m,vmm);

        return((*vmm)());
    }
};

//------------------------------------------------------------------------------

class DS1WMClose : public RCD::MenuItem {
  public:
    DS1WMClose() { setName("close DS1WM"); };
    int action() {

        // delete DS1WM
        delete module;

        // close VME master mapping
        RCD::VME::Close();

        return(0);
    }
};

//------------------------------------------------------------------------------

class DS1WMCommandRead : public RCD::MenuItem {
  public:
    DS1WMCommandRead() { setName("read  COMMAND"); };
    int action() {

        u_int data;
        int rtnv;

        if((rtnv = module->CommandRead(data)) == 0) {
            std::printf("data = %08x\n",data & 0xffffffff);
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class DS1WMCommandWrite : public RCD::MenuItem {
  public:
    DS1WMCommandWrite() { setName("write COMMAND"); };
    int action() {

        u_int data = enterHex("Command",0,0xffffffff);

        return(module->CommandWrite(data));
    }
};

//------------------------------------------------------------------------------

class DS1WMBufferRead : public RCD::MenuItem {
  public:
    DS1WMBufferRead() { setName("read  BUFFER"); };
    int action() {

        u_int data;
        int rtnv;

        if((rtnv = module->BufferRead(data)) == 0) {
            std::printf("data = %08x\n",data & 0xffffffff);
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class DS1WMBufferWrite : public RCD::MenuItem {
  public:
    DS1WMBufferWrite() { setName("write BUFFER"); };
    int action() {

        u_int data = enterHex("Buffer",0,0xffffffff);

        return(module->BufferWrite(data));
    }
};

//------------------------------------------------------------------------------

class DS1WMIntrStatusRead : public RCD::MenuItem {
  public:
    DS1WMIntrStatusRead() { setName("read  INTR_STATUS"); };
    int action() {

        u_int data;
        int rtnv;

        if((rtnv = module->IntrStatusRead(data)) == 0) {
            std::printf("data = %08x\n",data & 0xffffffff);
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class DS1WMIntrEnableRead : public RCD::MenuItem {
  public:
    DS1WMIntrEnableRead() { setName("read  INTR_ENABLE"); };
    int action() {

        u_int data;
        int rtnv;

        if((rtnv = module->IntrEnableRead(data)) == 0) {
            std::printf("data = %08x\n",data & 0xffffffff);
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class DS1WMIntrEnableWrite : public RCD::MenuItem {
  public:
    DS1WMIntrEnableWrite() { setName("write INTR_ENABLE"); };
    int action() {

        u_int data = enterHex("IntrEnable",0,0xffffffff);

        return(module->IntrEnableWrite(data));
    }
};

//------------------------------------------------------------------------------

class DS1WMClockDivRead : public RCD::MenuItem {
  public:
    DS1WMClockDivRead() { setName("read  CLOCK_DIV"); };
    int action() {

        u_int data;
        int rtnv;

        if((rtnv = module->ClockDivRead(data)) == 0) {
            std::printf("data = %08x\n",data & 0xffffffff);
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class DS1WMClockDivWrite : public RCD::MenuItem {
  public:
    DS1WMClockDivWrite() { setName("write CLOCK_DIV"); };
    int action() {

        u_int data = enterHex("ClockDiv",0,0xffffffff);

        return(module->ClockDivWrite(data));
    }
};

//------------------------------------------------------------------------------

class DS1WMControlRead : public RCD::MenuItem {
  public:
    DS1WMControlRead() { setName("read  CONTROL"); };
    int action() {

        u_int data;
        int rtnv;

        if((rtnv = module->ControlRead(data)) == 0) {
            std::printf("data = %08x\n",data & 0xffffffff);
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class DS1WMControlWrite : public RCD::MenuItem {
  public:
    DS1WMControlWrite() { setName("write CONTROL"); };
    int action() {

        u_int data = enterHex("Control",0,0xffffffff);

        return(module->ControlWrite(data));
    }
};

//------------------------------------------------------------------------------

class DS1WMReset : public RCD::MenuItem {
  public:
    DS1WMReset() { setName("reset OW bus"); };
    int action() {

        return(module->reset());
    }
};

//------------------------------------------------------------------------------

class DS1WMPresent : public RCD::MenuItem {
  public:
    DS1WMPresent() { setName("read  Presence Detect"); };
    int action() {

        bool prs;
        int rtnv;

        if((rtnv = module->present(prs)) == 0) {
            std::printf("Presence Detect: %sOW device present\n",(prs?"":"NO "));
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------

class DS1WMInit : public RCD::MenuItem {
  public:
    DS1WMInit() { setName("init  OW bus"); };
    int action() {

        return(module->init());
    }
};

//------------------------------------------------------------------------------

class DS1WMReadRom : public RCD::MenuItem {
  public:
    DS1WMReadRom() { setName("read  OW ROM"); };
    int action() {

        u_char rom[DS1WM::UCHAR_NUMBER];
        int rtnv;

        if((rtnv = module->readRom(rom)) == 0) {
            DS1WM::printRom(rom);
        }

        return(rtnv);
    }
};

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

int main() {

    RCD::Menu menu("DS1WM main menu");

    // menu for DS1WM
    menu.init(new DS1WMOpen);
    menu.add(new DS1WMReset);
    menu.add(new DS1WMPresent);
    menu.add(new DS1WMInit);
    menu.add(new DS1WMReadRom);

    // low-level menu
    RCD::Menu menuLow("LOW-LEVEL MENU");
    menuLow.add(new DS1WMCommandRead);
    menuLow.add(new DS1WMCommandWrite);
    menuLow.add(new DS1WMBufferRead);
    menuLow.add(new DS1WMBufferWrite);
    menuLow.add(new DS1WMIntrStatusRead);
    menuLow.add(new DS1WMIntrEnableRead);
    menuLow.add(new DS1WMIntrEnableWrite);
    menuLow.add(new DS1WMClockDivRead);
    menuLow.add(new DS1WMClockDivWrite);
    menuLow.add(new DS1WMControlRead);
    menuLow.add(new DS1WMControlWrite);
    menu.add(&menuLow);

    menu.exit(new DS1WMClose);

    // execute menu
    menu.execute();

    exit(0);
}
