//******************************************************************************
// file: DS1WM.cc
// desc: DS1WM Interface
// auth: 15-AUG-2008 R. Spiwoks
//******************************************************************************

#include <DS1WM/DS1WM.h>
#include "RCDUtilities/RCDUtilities.h"

#include <iostream>
#include <sstream>
#include <iomanip>

using namespace LVL1;

//------------------------------------------------------------------------------

const int DS1WM::CRC8_TABLE[256] = {
      0,  94, 188, 226,  97,  63, 221, 131, 194, 156, 126,  32, 163, 253,  31,  65,
    157, 195,  33, 127, 252, 162,  64,  30,  95,   1, 227, 189,  62,  96, 130, 220,
     35, 125, 159, 193,  66,  28, 254, 160, 225, 191,  93,   3, 128, 222,  60,  98,
    190, 224,   2,  92, 223, 129,  99,  61, 124,  34, 192, 158,  29,  67, 161, 255,
     70,  24, 250, 164,  39, 121, 155, 197, 132, 218,  56, 102, 229, 187,  89,   7,
    219, 133, 103,  57, 186, 228,   6,  88,  25,  71, 165, 251, 120,  38, 196, 154,
    101,  59, 217, 135,   4,  90, 184, 230, 167, 249,  27,  69, 198, 152, 122,  36,
    248, 166,  68,  26, 153, 199,  37, 123,  58, 100, 134, 216,  91,   5, 231, 185,
    140, 210,  48, 110, 237, 179,  81,  15,  78,  16, 242, 172,  47, 113, 147, 205,
     17,  79, 173, 243, 112,  46, 204, 146, 211, 141, 111,  49, 178, 236,  14,  80,
    175, 241,  19,  77, 206, 144, 114,  44, 109,  51, 209, 143,  12,  82, 176, 238,
     50, 108, 142, 208,  83,  13, 239, 177, 240, 174,  76,  18, 145, 207,  45, 115,
    202, 148, 118,  40, 171, 245,  23,  73,   8,  86, 180, 234, 105,  55, 213, 139,
     87,   9, 235, 181,  54, 104, 138, 212, 149, 203,  41, 119, 244, 170,  72,  22,
    233, 183,  85,  11, 136, 214,  52, 106,  43, 117, 151, 201,  74,  20, 246, 168,
    116,  42, 200, 150,  21,  75, 169, 247, 182, 232,  10,  84, 215, 137, 107,  53
};

const std::string DS1WM::MODULE_NAME[MODULE_NUMBER] = {
  "UNKNOWN", "MIOCT", "MIROD", "MICTP", "CTPCOREPLUS", "CTPOUTPLUS", "ALTI"
};

static const std::pair<const std::string, const std::string> _ds1wm_id_ [] = {
    std::make_pair("00000e543c0d","20DATMCTP00004"),
    std::make_pair("00000e5e59f3","20DATMCTP00005"),
    std::make_pair("00000e7785b2","20DATMCTP00006"),

    std::make_pair("00000e5499f6","20DATMOCT00003"),
    std::make_pair("00000eec95b4","20DATMOCT00004"),
    std::make_pair("00000eec93e8","20DATMOCT00005"),
    std::make_pair("00000e5e6cd1","20DATMOCT00008"),
    std::make_pair("00000eec9727","20DATMOCT00009"),
    std::make_pair("00000e5e5dc0","20DATMOCT00011"),
    std::make_pair("00000e5e6893","20DATMOCT00012"),
    std::make_pair("00000eecc912","20DATMOCT00015"),
    std::make_pair("00000e77874c","20DATMOCT00016"),
    std::make_pair("00000eeccbe7","20DATMOCT00017"),
    std::make_pair("00000e778e81","20DATMOCT00023"),
    std::make_pair("00000ee96f18","20DATMOCT00029"),
    std::make_pair("00000e5e5dd8","20DATMOCT00030"),
    std::make_pair("00000e54773d","20DATMOCT00031"),
    std::make_pair("00000e5e5767","20DATMOCT00032"),
    std::make_pair("00000ebc2aa7","20DATMOCT00033"),
    std::make_pair("00000e54661a","20DATMOCT00034"),
    std::make_pair("00000e5481fd","20DATMOCT00035"),
    std::make_pair("00000ebc21e0","20DATMOCT00036"),

    std::make_pair("00000eec96bb","20DATMROD00001"),
    std::make_pair("00000ebc22f1","20DATMROD00002"),
    std::make_pair("00000eec929c","20DATMROD00003"),

    std::make_pair("000019e0c1d8","20DATALTI00001"),
    std::make_pair("000019e0c526","20DATALTI00002"),
    std::make_pair("000019e0bd7e","20DATALTI00003"),
    std::make_pair("000019e0cca7","20DATALTI00004"),
    std::make_pair("00001aaaec2b","20DATALTI00005"),
    std::make_pair("00001aaaff3d","20DATALTI00006"),
    std::make_pair("00001aaa6579","20DATALTI00007"),

    // Pre-series ALTI moduels
    std::make_pair("00001aac8215","20DATALTI00008"),
    std::make_pair("00001aacb141","20DATALTI00009"),
    std::make_pair("00001aacc024","20DATALTI00010"),
    std::make_pair("00001aacc131","20DATALTI00011"),
    std::make_pair("00001aac9c45","20DATALTI00012"),
    std::make_pair("00001a0f9999","20DATALTI00013"),
    std::make_pair("00001a0fc194","20DATALTI00014"),
    std::make_pair("00001a0fc743","20DATALTI00015"),
    std::make_pair("00001a0f7d83","20DATALTI00016"),
    std::make_pair("00001a0f6ca0","20DATALTI00017"),
    std::make_pair("00001a0fc40e","20DATALTI00018"),
    std::make_pair("00001a0f9daf","20DATALTI00019"),
    std::make_pair("00001a0fb0b0","20DATALTI00020"),
    std::make_pair("00001a0fb4fc","20DATALTI00021"),
    std::make_pair("00001a0fc0e5","20DATALTI00022"),
    std::make_pair("00001a0f6ead","20DATALTI00023"),
    std::make_pair("00001a0fb1b0","20DATALTI00024"),
    std::make_pair("00001a0fc12c","20DATALTI00025"),
    std::make_pair("00001a0fbb59","20DATALTI00026"),
    std::make_pair("00001a0f797e","20DATALTI00027"),
    std::make_pair("00001a0fb0ea","20DATALTI00028"),
    std::make_pair("00001a0fc2c0","20DATALTI00029"),
    std::make_pair("00001a0fbv83","20DATALTI00030"),
    std::make_pair("00001a0f8d3f","20DATALTI00031"),
    std::make_pair("00001a0fbed4","20DATALTI00032"),
    
    // series ALTI modules
    std::make_pair("00001aef784a","20DATALTI00033"),
    std::make_pair("00001aef923f","20DATALTI00034"),
    std::make_pair("00001aef7d3c","20DATALTI00035"),
    std::make_pair("00001aef8596","20DATALTI00036"),
    std::make_pair("00001aef99d3","20DATALTI00037"),
    std::make_pair("00001aef7142","20DATALTI00038"),
    std::make_pair("00001aef8230","20DATALTI00039"),
    std::make_pair("00001aef8d8d","20DATALTI00040"),
    std::make_pair("00001aef7c42","20DATALTI00041"),
    std::make_pair("00001aef88c9","20DATALTI00042"),
    std::make_pair("00001aef8993","20DATALTI00043"),
    std::make_pair("00001aef92ed","20DATALTI00044"),
    std::make_pair("00001aef90c1","20DATALTI00045"),
    std::make_pair("00001aef8a41","20DATALTI00046"),
    std::make_pair("00001aef891b","20DATALTI00047"),
    std::make_pair("00001aef7130","20DATALTI00048"),
    std::make_pair("00001aefc27a","20DATALTI00049"),
    std::make_pair("00001aef81da","20DATALTI00050"),
    std::make_pair("00001aefc230","20DATALTI00051"),
    std::make_pair("00001aef9777","20DATALTI00052"),
    std::make_pair("00001aef8a12","20DATALTI00053"),
    std::make_pair("00001aef7cb5","20DATALTI00054"),
    std::make_pair("00001aef91bb","20DATALTI00055"),
    std::make_pair("00001aef98ba","20DATALTI00056"),
    std::make_pair("00001aef86f1","20DATALTI00057"),
    std::make_pair("00001aef80df","20DATALTI00058"),
    std::make_pair("00001aef7c12","20DATALTI00059"),
    std::make_pair("00001aef88c4","20DATALTI00060"),
    std::make_pair("00001aef7368","20DATALTI00061"),
    std::make_pair("00001aef7c71","20DATALTI00062"),
    std::make_pair("00001aef970b","20DATALTI00063"),
    std::make_pair("00001aef7afb","20DATALTI00064"),
    std::make_pair("00001aef7c50","20DATALTI00065"),
    std::make_pair("00001aef7d74","20DATALTI00066"),
    std::make_pair("00001aef93be","20DATALTI00067"),
    std::make_pair("00001aef8a47","20DATALTI00068"),
    std::make_pair("00001aef96ce","20DATALTI00069"),
    std::make_pair("00001aef9092","20DATALTI00070"),
    std::make_pair("00001aef9694","20DATALTI00071"),
    std::make_pair("00001af0afb9","20DATALTI00072"),
    std::make_pair("00001aef9123","20DATALTI00073"),
    std::make_pair("00001aef8c9b","20DATALTI00074"),
    std::make_pair("00001aef701b","20DATALTI00075"),
    std::make_pair("00001aef9820","20DATALTI00076"),
    std::make_pair("00001aef82c4","20DATALTI00077"),
    std::make_pair("00001aef90ee","20DATALTI00078"),
    std::make_pair("00001aef73b2","20DATALTI00079"),
    std::make_pair("00001aef95c7","20DATALTI00080"),
    std::make_pair("00001aef833d","20DATALTI00081")
};

const std::map<const std::string, const std::string> DS1WM::ID_MAP(_ds1wm_id_,_ds1wm_id_+sizeof(_ds1wm_id_)/sizeof(_ds1wm_id_[0]));

//------------------------------------------------------------------------------

DS1WM::DS1WM(MODULE_TYPE mod, RCD::VMEMasterMap* vmm) {

    // user externally created VMEbus master mapping
    m_vmm = vmm;

    switch (mod) {
    case MIOCT:
        m_base = 0x00080020;
        break;
    case MIROD:
        m_base = 0x00020c00;
        break;
    case MICTP:
        m_base = 0x00020c00;
        break;
    case CTPCOREPLUS:
        m_base = 0x00200100;
        break;
    case CTPOUTPLUS:
        m_base = 0x00000200;
        break;
    case ALTI:
        m_base = 0x000080a0;
        break;
    default:
        CERR("Unknown module type \"%d\"", (int)mod);
        return;
    }
    m_command     = m_base + ADDR_COMMAND;
    m_buffer      = m_base + ADDR_BUFFER;
    m_intr_status = m_base + ADDR_INTR_STATUS;
    m_intr_enable = m_base + ADDR_INTR_ENABLE;
    m_clock_div   = m_base + ADDR_CLOCK_DIV;
    m_control     = m_base + ADDR_CONTROL;

    // report
    std::printf("DS1WM::DS1WM: INFO - opened DS1WM of type \"%s\" at offset 0x%08x\n",MODULE_NAME[mod].c_str(),m_base);
}

//------------------------------------------------------------------------------

DS1WM::~DS1WM() {

    // nothing to be done
}

//------------------------------------------------------------------------------

int DS1WM::CommandRead(u_int& data) {

    return(m_vmm->ReadSafe(m_command,&data));
}

//------------------------------------------------------------------------------

int DS1WM::CommandWrite(u_int data) {

    return(m_vmm->WriteSafe(m_command,data));
}

//------------------------------------------------------------------------------

int DS1WM::BufferRead(u_int& data) {

    return(m_vmm->ReadSafe(m_buffer,&data));
}

//------------------------------------------------------------------------------

int DS1WM::BufferWrite(u_int data) {

    return(m_vmm->WriteSafe(m_buffer,data));
}

//------------------------------------------------------------------------------

int DS1WM::IntrStatusRead(u_int& data) {

    return(m_vmm->ReadSafe(m_intr_status,&data));
}

//------------------------------------------------------------------------------

int DS1WM::IntrEnableRead(u_int& data) {

    return(m_vmm->ReadSafe(m_intr_enable,&data));
}

//------------------------------------------------------------------------------

int DS1WM::IntrEnableWrite(u_int data) {

    return(m_vmm->WriteSafe(m_intr_enable,data));
}

//------------------------------------------------------------------------------

int DS1WM::ClockDivRead(u_int& data) {

    return(m_vmm->ReadSafe(m_clock_div,&data));
}

//------------------------------------------------------------------------------

int DS1WM::ClockDivWrite(u_int data) {

    return(m_vmm->WriteSafe(m_clock_div,data));
}

//------------------------------------------------------------------------------

int DS1WM::ControlRead(u_int& data) {

    return(m_vmm->ReadSafe(m_control,&data));
}

//------------------------------------------------------------------------------

int DS1WM::ControlWrite(u_int data) {

    return(m_vmm->WriteSafe(m_control,data));
}

//------------------------------------------------------------------------------

int DS1WM::reset() {

    u_int data;
    int rtnv;

    // write RESET to COMMAND register
    data = CMD_1WR;
    if((rtnv = CommandWrite(data)) != SUCCESS) {
        CERR("writing RESET to COMMAND register","");
        return(rtnv);
    }

    // wait for RESET to complete
    if(waitReset() != SUCCESS) {
        CERR("waiting for RESET to complete","");
        return(rtnv);
    }

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int DS1WM::present(bool& prs) {

    u_int data;
    int rtnv;

    // read INTR_STATUS register
    if((rtnv = IntrStatusRead(data)) != SUCCESS) {
        CERR("reading INTR_STATUS register","");
        return(rtnv);
    }

    prs = data & IS_PD;

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int DS1WM::init() {

    int rtnv;

    // set clock division to 40 MHz
    if((rtnv = ClockDivWrite(CLOCK_DIVISION)) != SUCCESS) {
        CERR("writing CLOCK_DIV register","");
        return(rtnv);
    }

    // reset
    if((rtnv = reset()) != SUCCESS) {
        CERR("resetting 1W bus","");
        return(rtnv);
    }

    // checking present detect
    bool prs;
    if((rtnv = present(prs)) != SUCCESS) {
        CERR("reading PRESENT_DETECT","");
        return(rtnv);
    }
    if(!prs) {
        CERR("no 1W device present","");
        return(rtnv);
    }

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int DS1WM::readRom(u_char rom[UCHAR_NUMBER]) {

    u_int data;
    int i;
    int rtnv;
    u_char crc8a = 0x00;
    u_char crc8b = 0x00;

    // write READ_ROM command
    data = OW_READ_ROM;
    if((rtnv = transfer((u_char)data,(u_char&)data)) != SUCCESS) {
        CERR("writing READ_ROM command","");
        return(rtnv);
    }

    // write/read eight bytes
    for(i=0; i<UCHAR_NUMBER; i++) {
        if((rtnv = transfer((u_char)OW_MASK,rom[i])) != SUCCESS) {
            CERR("reading byte #%d",i);
            return(rtnv);
        }
        crc8a = CRC8_TABLE[crc8a] ^ rom[i];
        crc8b = crc8(rom[i],crc8b);
        CDBG("crc8a = 0x%02x, crc8b = 0x%02x",crc8a,crc8b);
    }

    if((crc8a != 0x00) || (crc8b != 0x00) || (crc8a != crc8b)) {
        std::ostringstream buf; buf << std::hex << std::setfill('0');
        for(i=(UCHAR_NUMBER-1); i>=0; i--) buf << std::setw(2) << (unsigned int)rom[i];
        CERR("wrong CRC8 = 0x%02x (A), 0x%02x (B), ROM = 0x%s",crc8a,crc8b,buf.str().c_str());
        return(-1);
    }

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int DS1WM::checkRom(unsigned char rom[UCHAR_NUMBER]) {

    unsigned int data;
    int i, rtnv;
    unsigned char crc8a(0x00), crc8b(0x00);

    // write READ_ROM command
    data = OW_READ_ROM;
    if((rtnv = transfer((u_char)data,(u_char&)data)) != SUCCESS) {
        return(rtnv);
    }

    // write/read eight bytes
    for(i=0; i<UCHAR_NUMBER; i++) {
        if((rtnv = transfer((u_char)OW_MASK,rom[i])) != SUCCESS) {
            return(rtnv);
        }
        crc8a = CRC8_TABLE[crc8a] ^ rom[i];
        crc8b = crc8(rom[i],crc8b);
    }

    if((crc8a != 0x00) || (crc8b != 0x00) || (crc8a != crc8b)) {
        return(FAILURE);
    }

    return(SUCCESS);
}

//------------------------------------------------------------------------------

u_char DS1WM::crc8(u_char dat, u_char crc) {

    int i;

    for(i=0; i<UCHAR_NUMBER; i++) {
        if(((dat >> i) & 0x01) != (crc &0x01)) {
            crc = (crc >> 1) ^ CRC8_POLY;
        }
        else {
            crc = crc >> 1;
        }
        CDBG("dat[%d] = 0x%x => crc8b = 0x%02x",i,(dat>>i)&0x01,crc);
    }

    return(crc);
}
//------------------------------------------------------------------------------

int DS1WM::waitReset() {

    u_int data;
    int loop = 0;
    int rtnv;

    // wait for reset to complete
    while(true) {
        if((rtnv = CommandRead(data)) != SUCCESS) {
            CERR("reading COMMAND register","");
            return(rtnv);
        }
        if((data & CMD_1WR) != CMD_1WR) break;
        if(++loop == TIMEOUT) {
            CERR("timeout waiting for reset to complete","");
            return(-1);
        }
    }
    CDBG("returning after loop = %d",loop);
    return(SUCCESS);
}

//------------------------------------------------------------------------------

int DS1WM::transfer(u_char in, u_char& out) {

    u_int data;
    int rtnv;

    // write one byte
    if((rtnv = BufferWrite(in)) != SUCCESS) {
        CERR("writing to BUFFER register","");
        return(rtnv);
    }

    // wait for transfer to complete
    if((rtnv = waitTransfer()) != SUCCESS) {
        CERR("waiting for transfer to complete","");
        return(rtnv);
    }

    // read one byte
    if((rtnv = BufferRead(data)) != SUCCESS) {
        CERR("reading from BUFFER register","");
        return(rtnv);
    }
    out = data & OW_MASK;

    return(SUCCESS);
}

//------------------------------------------------------------------------------

int DS1WM::waitTransfer() {

    u_int data;
    int loop = 0;
    int rtnv;

    // wait for transfer to complete
    while(true) {
        if((rtnv = IntrStatusRead(data)) != SUCCESS) {
            CERR("reading INTR_STATUS register","");
            return(rtnv);
        }
        if((data & IS_READY) == IS_READY) break;
        if(++loop == TIMEOUT) {
            CERR("timeout waiting for transfer to complete","");
            return(-1);
        }
    }

    return(SUCCESS);
}

//------------------------------------------------------------------------------

std::string DS1WM::idRom(u_char rom[UCHAR_NUMBER]) {

    std::ostringstream buf;
    int i;

    buf << std::hex << std::setfill('0');
    for(i=(UCHAR_NUMBER-2); i>=1; i--) buf << std::setw(2) << (u_int)rom[i];
    return(buf.str());
}

//------------------------------------------------------------------------------

void DS1WM::printRom(u_char rom[UCHAR_NUMBER]) {

    int i;

    std::printf("CRC8 = %02x, ID = ",(u_int)rom[DS1WM::UCHAR_NUMBER-1]);
    for(i=UCHAR_NUMBER-2; i>=1; i--) std::printf(" %02x",(u_int)rom[i]);
    std::printf(", FAMILY = %02x\n",(u_int)rom[0]);
}

//------------------------------------------------------------------------------

int DS1WM::getSerialNumber(std::string& serial) {

    int rtnv;

    if((rtnv = reset()) != 0) {
        CERR("resetting DS1WM","");
        return(rtnv);
    }

    u_char rom[DS1WM::UCHAR_NUMBER];
    if((rtnv = readRom(rom)) != 0) {
        CERR("reading OW ROM","");
        return(rtnv);
    }

    if(familyRom(rom) != FAMILY_ROM) {
        CERR("read wrong OW family number 0x%02x - expected 0x%02x",familyRom(rom),FAMILY_ROM);
        return(FAILURE);
    }
    serial = idRom(rom);

    return(rtnv);
}

//------------------------------------------------------------------------------

int DS1WM::detectSerialNumber(std::string& serial) {

    int rtnv;

    // set clock division to 40 MHz
    if((rtnv = ClockDivWrite(CLOCK_DIVISION)) != SUCCESS) return(rtnv);

    // reset
    if((rtnv = reset()) != SUCCESS) return(rtnv);

    // check present detect
    bool prs;
    if((rtnv = present(prs)) != SUCCESS) return(rtnv);
    if(!prs) return(FAILURE);

    // check ROM
    unsigned char rom[UCHAR_NUMBER];
    if((rtnv = checkRom(rom)) != 0) return(rtnv);

    // check FAMILY
    if(familyRom(rom) != FAMILY_ROM) return(FAILURE);

    // return serial number
    serial = idRom(rom);

    return(SUCCESS);
}

//------------------------------------------------------------------------------

void DS1WM::getIdentifier(const std::string& ser, std::string& id) {

    std::map<const std::string, const std::string>::const_iterator pos;

    if((pos = ID_MAP.find(ser)) != ID_MAP.end()) {
        id = pos->second.c_str();
    }
    else {
        std::ostringstream ids;
        ids << "<UNIDENTIFIED";
        if(ser != "") ids << " = 0x" << ser;
        ids << ">";
        id = ids.str();
    }
}
