#ifndef LVL1_DS1WM_H
#define LVL1_DS1WM_H

//******************************************************************************
// file: DS1WM.h
// desc: DS1WM Interface
// auth: 15-AUG-2008 R. Spiwoks
//******************************************************************************

#include <map>

#include "RCDVme/RCDVme.h"

namespace LVL1 {

  class DS1WM {

  public:

    // public type
    typedef enum {UNKNOWN, MIOCT, MIROD, MICTP, CTPCOREPLUS, CTPOUTPLUS, ALTI} MODULE_TYPE;
    static const int MODULE_NUMBER = ALTI + 1;
    static const std::string MODULE_NAME[MODULE_NUMBER];

    // public constant
    static const int    UCHAR_NUMBER            = 8;
    static const u_char FAMILY_ROM              = 0x01;
    static const std::map<const std::string, const std::string> ID_MAP;

    // public constructor and desctructor
    DS1WM(MODULE_TYPE,RCD::VMEMasterMap*);
   ~DS1WM();

    // public register functions
    int CommandRead(u_int&);
    int CommandWrite(u_int);

    int BufferRead(u_int&);
    int BufferWrite(u_int);

    int IntrStatusRead(u_int&);

    int IntrEnableRead(u_int&);
    int IntrEnableWrite(u_int);

    int ClockDivRead(u_int&);
    int ClockDivWrite(u_int);

    int ControlRead(u_int&);
    int ControlWrite(u_int);

    // public high-level functions
    int reset();
    int present(bool&);
    int init();
    int readRom(u_char[UCHAR_NUMBER]);
    int checkRom(unsigned char[UCHAR_NUMBER]);
    int getSerialNumber(std::string&);
    int detectSerialNumber(std::string&);
    static void getIdentifier(const std::string&, std::string&);

    // public helpers for ROM array
    static u_char familyRom(u_char rom[UCHAR_NUMBER])  { return(rom[0]); }
    static std::string idRom(u_char rom[UCHAR_NUMBER]);
    static u_char crc8Rom(u_char rom[UCHAR_NUMBER])     { return(rom[UCHAR_NUMBER-1]); }
    static void printRom(u_char[UCHAR_NUMBER]);

  private:

    // constants
    static const u_int  ADDR_COMMAND            = 0x00000000;
    static const u_int  ADDR_BUFFER             = 0x00000004;
    static const u_int  ADDR_INTR_STATUS        = 0x00000008;
    static const u_int  ADDR_INTR_ENABLE        = 0x0000000c;
    static const u_int  ADDR_CLOCK_DIV          = 0x00000010;
    static const u_int  ADDR_CONTROL            = 0x00000014;

    static const u_int  CMD_1WR                 = 0x00000001;
    static const u_int  CMD_SRA                 = 0x00000002;
    static const u_int  CMD_FOW                 = 0x00000004;
    static const u_int  CMD_OW_IN               = 0x00000008;
    static const u_int  IS_PD                   = 0x00000001;
    static const u_int  IS_PDR                  = 0x00000002;
    static const u_int  IS_TBE                  = 0x00000004;
    static const u_int  IS_TEMT                 = 0x00000008;
    static const u_int  IS_RBF                  = 0x00000010;
    static const u_int  IS_RSRF                 = 0x00000020;
    static const u_int  IS_READY                = IS_TBE | IS_TEMT | IS_RBF;

    static const u_int  OW_READ_ROM             = 0x00000033;
    static const u_int  OW_MASK                 = 0x000000ff;

    static const int    TIMEOUT                 =       1000;
    static const int    SUCCESS                 =          0;
    static const int    FAILURE                 =         -1;

    static const u_int  CLOCK_DIVISION          = 0x0000008e;
    static const int    CRC8_TABLE[256];
    static const u_char CRC8_POLY               =       0x8c;

    // private data members
    RCD::VMEMasterMap*          m_vmm;
    u_int                       m_base;
    u_int                       m_command;
    u_int                       m_buffer;
    u_int                       m_intr_status;
    u_int                       m_intr_enable;
    u_int                       m_clock_div;
    u_int                       m_control;
    int                         m_rtnv;

    // private methods
    int waitReset();
    int transfer(u_char, u_char&);
    int waitTransfer();
    u_char crc8(u_char, u_char);

};     // class DS1WM

}      // namespace LVL1

#endif // LVL1_DS1WM_H
