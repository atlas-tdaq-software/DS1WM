#package DS1WM

#author  Ralf.Spiwoks@cern.ch
#manager Ralf.Spiwoks@cern.ch

tdaq_package()

#set(CMAKE_VERBOSE_MAKEFILE on)

tdaq_add_library(DS1WM src/DS1WM.cc
  LINK_LIBRARIES RCDVme)

tdaq_add_executable(menuDS1WM src/test/menuDS1WM.cc 
  LINK_LIBRARIES RCDMenu DS1WM)
